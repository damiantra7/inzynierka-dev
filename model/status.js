const { Sequelize, DataTypes, Model } = require('sequelize')

module.exports = (sequelize) => {
    sequelize.define('Status',
        {
            id:
            {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            nazwa:
            {
                type: DataTypes.STRING,
                allowNull: false
            }
        },
        {
            freezeTableName:true
        })
}