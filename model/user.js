const sequelize = require('sequelize');
const {Sequelize,DataTypes, Model} = require('sequelize');
module.exports = (sequelize) =>
{
    sequelize.define('Uzytkownik',{
        id:{
            type:DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        imie: {
            type: DataTypes.STRING,
            allowNull:false
        },
        nazwisko:{
           type:DataTypes.STRING,
            allowNull:false
        },
        prywatny_email:{
            type:DataTypes.STRING,
            allowNull:false
        },
        biznes_mail:
        {
            type:DataTypes.STRING,
            allowNull:false
        },
        numer_tel:
        {
            type:DataTypes.STRING,
            allowNull:false
        },
        haslo:
        {
            type:DataTypes.STRING,
            allowNull:false
        },
        salt:
        {
            type:DataTypes.STRING,
            allowNull:false
        },
        role_id:
        {
            type:DataTypes.INTEGER,
            allowNull:false,
            references:
            {
                model:'Role',
                key:'id'
            }
        },
        address_id:
        {
            type:DataTypes.INTEGER,
            allowNull:false,
            references:
            {
                model:'Adres',
                key:'id'
            }
        },
        zdjecie:
        {
            type:DataTypes.BLOB,
            allowNull:true,
        }

    },
    {
        freezeTableName:true
    })
}