const { Sequelize, DataTypes, Model } = require('sequelize')
module.exports = (sequelize) => {
    sequelize.define("Adres", {
        id:
        {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        nr_domu:
        {
            type: DataTypes.STRING,
            allowNull: false
        },
        nr_mieszkania:
        {
            type: DataTypes.STRING,
            allowNull: true
        },
        ulica:
        {
            type: DataTypes.STRING,
            allowNull: false
        },
        kod_pocztowy:
        {
            type: DataTypes.STRING,
            allowNull: false
        },
        miasto:
        {
            type: DataTypes.STRING,
            allowNull: false
        },
        wojewodztwo:
        {
            type: DataTypes.STRING,
            allowNull: false
        }
    },
    {
        freezeTableName:true
    })

}