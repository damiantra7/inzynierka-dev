const { Sequelize, DataTypes, Model } = require('sequelize')
module.exports = (sequelize) => {
    sequelize.define('Pojazdy', {
        id:
        {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        marka:
        {
            type: DataTypes.STRING,
            allowNull: false
        },
        numer_VIN:
        {
            type: DataTypes.STRING,
            allowNull: false
        },
        numer_rejestracyjny:
        {
            type: DataTypes.STRING,
            allowNull: false
        },
        status_pojazdu:
        {
            type: DataTypes.STRING,
            allowNull: false
        },
        note:
        {
            type: DataTypes.TEXT,
            allowNull: true
        }
    },
    {
        freezeTableName:true
    })
}