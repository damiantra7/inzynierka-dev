const sequelize = require('sequelize')
const { Sequelize, DataTypes, Model } = require('sequelize')
module.exports = (sequelize) =>
{
    sequelize.define("Rezerwacja_pojazdów",{
        id:
        {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        dzien_rezerwacji:
        {
            type:DataTypes.DATE,
            allowNull:false 
        },
        pojazdy_id:
        {
            type: DataTypes.INTEGER,
            allowNull:false,
            references:
            {
                model: 'Pojazdy',
                key: 'id'
            }
        },
        uzytkownik_id:
        {
            type: DataTypes.INTEGER,
            allowNull:false,
            references:
            {
                model:'Uzytkownik',
                key: 'id'
            }
        }
    },{
        freezeTableName:true
    })
}