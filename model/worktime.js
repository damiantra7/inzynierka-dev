const { Sequelize, DataTypes, Model } = require('sequelize')
module.exports = (sequelize) => {
    sequelize.define('Worktime',
    {
        id:
        {
            type:DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        czas_rozpoczecia:
        {
            type:DataTypes.DATE,
            allowNull:false
        },
        czas_zakonczenia:
        {
            type:DataTypes.DATE,
            allowNull:true
        },
        uzytkownik_id:
        {
            type:DataTypes.INTEGER,
            allowNull:false,
            references:
            {
                model:'Uzytkownik',
                key:'id'
            }
        }

    },
    {
        freezeTableName:true
    })
}