const mongoose = require("mongoose")

const pokojeSchema = new mongoose.Schema({
    nazwa:{type:String,required:true},
    wiadomosci:
    [
        {
            uzytkownik:String,
            wiadomosc:String
        }
    ]
})
const Pokoje = new mongoose.model("Pokoje",pokojeSchema)

module.exports = {Pokoje}