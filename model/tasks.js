const { Sequelize, DataTypes, Model } = require('sequelize')

module.exports = (sequelize) =>
{
    sequelize.define('Zadania', {
        id:
        {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        nazwa:
        {
            type: DataTypes.STRING,
            allowNull: false
        },
        opis:
        {
            type: DataTypes.TEXT,
            allowNull: true
        },
        priorytet:
        {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        powiadomienia:
        {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        category_id:
        {
            type:DataTypes.INTEGER,
            allowNull:false,
            references:
            {
                model:'Kategoria',
                key:'id'
            }
        }
    },
    {
        freezeTableName:true
    })
}