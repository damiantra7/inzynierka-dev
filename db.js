const mariadb = require('mariadb')
const { Sequelize, Model } = require('sequelize');
db = {};
const models = [
    require('./model/adres'),
    require('./model/category'),
    require('./model/role'),
    require('./model/status'),
    require('./model/tasks'),
    require('./model/user'),
    require('./model/vehicles'),
    require('./model/vehicle_reservation'),
    require('./model/user_has_tasks'),
    require('./model/worktime')
];

const sequelize = new Sequelize('','root','student',{
    dialect:'mariadb',
    dialectOptions: {
        host: "mariadbinzynierka.cywznszttojn.eu-central-1.rds.amazonaws.com",
        port: 3306,
        user:"admin",
        password:"inzynierka",
        database:"Inzynierka"
    },
    logging:console.log
}); 
sequelize.authenticate()
.then(() => {
    console.log("Polaczono z baza danych")
})
.catch(err => {
    console.error("Nie udalo polaczyc sie z baza danych: ",err)
})
for (const model of models)
{
    model(sequelize);
}
sequelize.sync({force:false})
module.exports = sequelize