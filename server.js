const apm = require('elastic-apm-node').start({
    serviceName: 'Inzynierka',
    serverUrl: 'http://34.123.211.48:8200',
    seretToken: '',
    environment:'development'
})
const express = require('express')
require('dotenv').config()

const app = express()
const cors = require('cors')
const {createServer} =require("http")
const {Server} = require('socket.io')
const connection = require('./mongodb')
connection()
const port=8000
const {Pokoje} = require('./model/pokoje')
const sequelize = require('./db')
const bodyParser= require('body-parser')

const vehiclesRoutes = require('./controllers/vehiclesController')
const authRoutes = require('./controllers/authController')
const userRoutes = require('./controllers/userController')
const roleRoutes = require('./controllers/roleController')
const categoryRoutes = require('./controllers/categoryController')
const addressRoutes = require('./controllers/addressConroller')
const statusRoutes = require('./controllers/statusController')
const vehicles_reservationRoutes= require('./controllers/vehicle_reservationController')
const worktimeRoutes = require('./controllers/worktimeController')
const taskRoutes = require('./controllers/tasksController')
const usersTasks = require('./controllers/usersTasksController')
const chatRoutes = require('./controllers/chatController')
const contactBookRoutes = require('./controllers/contactBookController')
const mechanicMiddleware = require('./middleware/mechanicmiddleware')
const genericMiddleware = require('./middleware/loginmiddleware')
const adminMiddleware = require('./middleware/adminmiddleware')
const managerMiddleware = require('./middleware/managermiddleware')
const driverMiddleware = require('./middleware/drivermiddleware');
const simpleUserRoutes = require('./controllers/simpleUserController') 
const taskListRoutes = require('./controllers/taskListController')

const { Socket } = require('socket.io');
const { mongo } = require('mongoose')
const { MongoGridFSChunkError } = require('mongodb')
const { compare } = require('bcrypt')

app.use(express.json())
app.use(cors())

app.use("/api/auth",authRoutes)
app.use("/api/vehicles",mechanicMiddleware)
app.use("/api/vehicles",vehiclesRoutes)
app.use("/api/users",adminMiddleware)
app.use("/api/users",userRoutes)
app.use("/api/tasks",managerMiddleware)
app.use("/api/tasks",taskRoutes)
app.use("/api/userTasks",managerMiddleware)
app.use("/api/userTasks",usersTasks)
app.use("/api/roles",adminMiddleware)
app.use("/api/roles",roleRoutes)
app.use("/api/category",genericMiddleware)
app.use("/api/category",categoryRoutes)
app.use("/api/vehicles_reservation",driverMiddleware)
app.use("/api/vehicles_reservation",vehicles_reservationRoutes)
app.use("/api/worktime",genericMiddleware)
app.use("/api/worktime",worktimeRoutes)
app.use("/api/status",genericMiddleware)
app.use("/api/status",statusRoutes)
app.use("/api/address",genericMiddleware)
app.use("/api/address",addressRoutes)
app.use("/api/chat",genericMiddleware)
app.use("/api/chat",chatRoutes)
app.use("/api/contact",genericMiddleware)
app.use("/api/contact",contactBookRoutes)
app.use("/api/suser",genericMiddleware)
app.use("/api/suser",simpleUserRoutes)
app.use("/api/ltasks",genericMiddleware)
app.use("/api/ltasks",taskListRoutes)
//const server = app.listen(port,() => {
   // console.log(`Serwer slucha na  porcie ${port}`)
//})
const httpServer = createServer(app)
const io = new Server(httpServer,{allowEIO3:true,cors:{origin:"*",}});

io.on('connection',(socket)=>{
    socket.on('join',(data)=>{
        socket.join(data.nazwa)
        Pokoje.find({},(error,tab)=>{
            if(error)
            {
                console.log(error);
                return false;
            }
            let istnieje=false
            tab.forEach(pokoj => {
                if(pokoj.nazwa == data.nazwa)
                {
                    istnieje=true;
                }
            });
            if(!istnieje)
            {
                console.log("Nie istnieje")
                Pokoje({nazwa:data.nazwa,wiadomosci:[]}).save();
            }
         });
    });
    socket.on('message',(data)=>{
        io.in(data.pokoj).emit('new message',{uzytkownik:data.uzytkownik,wiadomosc:data.wiadomosc})
        console.log(data)
        Pokoje.updateOne({nazwa:data.nazwa},{$push: {wiadomosci:{uzytkownik:data.uzytkownik,wiadomosc:data.wiadomosc}}},(err,res)=>{
            if(err)
            {
                console.log(err);
                return false;
            }
        });
    });
    socket.on('typing',(data)=>{
        console.log(data)
        socket.broadcast.in(data.nazwa).emit('typing',{data:data,isTyping: true})
    })
})
httpServer.listen(port)
