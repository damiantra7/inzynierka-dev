const router = require('express').Router()
const { response } = require('express')
const sequelize = require('../db')
router.post('/',async(req,res)=>{
    const nazwa = req.body.nazwa
    sequelize.models.Kategoria.create({
        nazwa:nazwa    
    })
    res.status(200).send({message:'Dodano nowa kategorie'})
})
router.put('/:id',async(req,res)=>{
    id=req.params.id
    try
    {
        const nazwa = req.body.name
        sequelize.models.Kategoria.upsert({
            id:id,
            nazwa:nazwa
        })
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})
router.get("/",async(req,res) => {
   try
   {
        categories=await sequelize.models.Kategoria.findAll()
        req.status(200).send({data:categories,message:"Pobrano liste rol"})
   }
   catch(error)
   {
    res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
   }
})
router.get("/:id",async(req,res) => {
    id=req.params.id
    try
    {   
        Kategoria=await sequelize.models.Kategoria.findByPK(id)
        req.status(200).send({data:Kategoria,message:"Pobrano Kategoria"})

    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})
router.delete("/:id",async(req,res)=>{
    id=req.params.id
    try
    {
        const Kategoria = await sequelize.models.Kategoria.findByPK(id)
        Kategoria.destroy()
        res.status(200).send({message:"Pomyślnie usunieto uzytkownika"})
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})

module.exports = router