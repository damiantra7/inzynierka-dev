const router = require('express').Router()
const sequelize = require('../db')
router.post("/",async(req,res)=>{
    const marka = req.body.marka
    const numer_VIN = req.body.numer_VIN
    const numer_rejestracyjny = req.body.numer_rejestracyjny
    const status_pojazdu = req.body.status_pojazdu
    try
    {
        sequelize.models.Pojazdy.create({marka:marka,numer_VIN:numer_VIN,numer_rejestracyjny,status_pojazdu:status_pojazdu})
        //await Pojazdy.create({marka:mar 

        res.status(200).send({message:"Pomyslnie dodano pojazd do bazy danych"})
        console.log("Dziala")
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
    
})
router.delete("/:id",async(req,res)=>{
    const id = req.params.id
    try
    {
       const pojazd = await sequelize.models.Pojazdy.findByPk(id)
       pojazd.destroy()
        res.status(200).send({message:"Pomyslnie usunieto pojazd z bazy danych"})
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})
router.get("/",async(req,res) =>
{
    try
    {
        const pojazdy = await sequelize.models.Pojazdy.findAll();
        res.status(200).send({data:pojazdy,message:"PObrano liste pojazdow"});
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})
router.get("/:id",async(req,res) => {
    try
    {
        const id = req.params.id
        const pojazd = await sequelize.models.Pojazdy.findByPk(id)
        res.status(200).send({data:pojazd,message:"PObrano pojazd"});
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})
router.put("/:id",async(req,res)=>{
    id=req.params.id
    try
    {
        const marka = req.body.marka
        const numer_VIN = req.body.numer_VIN
        const numer_rejestracyjny = req.body.numer_rejestracyjny
        const status_pojazdu = req.body.status_pojazdu
        sequelize.models.Pojazdy.upsert({
            id:id,
            marka:marka,
            numer_VIN:numer_VIN,
            numer_rejestracyjny:numer_rejestracyjny,
            status_pojazdu:status_pojazdu
        })
        res.status(200).send({message:"Pomyslnie zaktualizowano pojazd"})
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})

module.exports = router