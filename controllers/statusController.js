const router = require('express').Router()
const { response } = require('express')
const sequelize = require('../db')

router.get('/',async(req,res)=>{
    try{
        statuses=await sequelize.models.Status.findAll()
        res.status(200).send({data:statuses,message:"Pobrano statusy"})
    } 
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
     
},
router.get("/:id",async(req,res)=>{
    id=req.params.id
    try
    {
        statuss=sequelize.models.Status.findByPK(id)
        res.status(200).send({data:statuses,message:"Pobrano status"})
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})
)
module.exports = router