const router = require('express').Router()
const sequelize = require('../db')
const { route } = require('./vehiclesController')
const jwt =require('jsonwebtoken')
const tasks = require('../model/tasks')
router.post("/",async(req,res)=>{
    const nazwa = req.body.nazwa
    const opis = req.body.opis
    const priorytet = req.body.priorytet
    const powiadomienia = req.body.powiadomienia
    const category = req.body.category
    try
    {
        const kategoria = await sequelize.models.Kategoria.findOne({where:{nazwa:category}})
        console.log(kategoria.id)
    
        const zadanie =await sequelize.models.Zadania.create({
        nazwa:nazwa,
        opis:opis,
        priorytet:priorytet,
        powiadomienia:powiadomienia,
        category_id:kategoria.id    
        })
        //await Pojazdy.create({marka:mar 
        res.status(200).send({message:"Pomyslnie dodano zadaine do bazy danych",id:zadanie.id})
        console.log("Dziala")
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
    
})
router.put("/",async(req,res)=>{
    const id =req.body.id
    const nazwa = req.body.nazwa
    const opis = req.body.opis
    const priorytet = req.body.priorytet
    const powiadomienia = req.body.powiadomienia
    const category = req.body.categoryy
    try
    {
        const kategoria = sequelize.models.Kategoria.findOne({where:{nazwa:category}})
        sequelize.models.Zadania.upsert({
        id:id,
        nazwa:nazwa,
        opis:opis,
        priorytet:priorytet,
        powiadomienia:powiadomienia,
        category:kategoria.id    
        })
        //await Pojazdy.create({marka:mar 

        res.status(200).send({message:"Pomyslnie zmodyfikowano zadanie"})
        console.log("Dziala")
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
    
})
router.delete("/:id",async(req,res) => {
    const id =  req.params.id
    try
    {
        const task = await sequelize.models.Zadania.findByPk(id)
        task.destroy()
        res.status(200).send({message:"Pomyslnie usunieto zadanie"})
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})
router.get("/",async(req,res) => {
    try 
    {
        tasks = await sequelize.models.Zadania.findAll()
        res.status(200). send({data:tasks,message:"Pomyslnie pobrano liste zadan"})
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})
router.get("/:id",async(req,res) => {
    id = req.params.id
    try
    {   
        task = await sequelize.models.Zadania.findByPk(id)
        res.status(200).send({data:task,message:"Pomyslnie pobrano zadania"})
    }
    catch(error)
    {
         res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})

router.delete("/enhanced/test",async(req,res) => {
    id=req.body.id
    zadania_id=req.body.zadania_id
    try
    {
        const userTasks = await sequelize.models.Zadania_uzytkownika.findByPk(id)
        userTasks.destroy();
        const task =await sequelize.models.Zadania.findByPk(zadania_id)
        task.destroy
        res.status(200).send({message:"Pomyslnie usunieto zadanie"})
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    } 
})

module.exports = router