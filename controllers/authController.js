const router = require('express').Router()
const sequelize = require('../db')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

router.post("/",async(req,res) => {
    const biznes_mail = req.body.biznes_mail
    try
    {
    const user= await sequelize.models.Uzytkownik.findOne({
        where:{
            biznes_mail:biznes_mail
        }
    })
    const role=await sequelize.models.Role.findOne({
        where:{
            id:user.role_id
        }
    })
    if(!user)
    {
        return res.status(401).send({message:"Błedny email lub hasło!"});   
    }   
    const poprawneHaslo= await bcrypt.compare(req.body.haslo,user.haslo)
    if(!poprawneHaslo)
    {
        return res.status(401).send({message:"Bledny email lub haslo"})
    }
    const token = jwt.sign({id:user.id,role:role.nazwa},process.env.KLUCZ_JWT,{expiresIn:"1d"})
     res.status(200).send({data:token,nazwa:user.imie+" "+user.nazwisko,email:user.biznes_mail,role:role.nazwa,message:"Zalogowano!"})
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
    
})
router.get("/",async(req,res) => {
    res.status(200).send({message:"Dziala"})
})

module.exports = router