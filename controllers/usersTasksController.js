const router = require('express').Router()
const sequelize = require('../db')
const bcrypt = require('bcrypt')
const user = require('../model/user')
const jwt = require('jsonwebtoken')
const { use } = require('../middleware/loginmiddleware')
const { json } = require('body-parser')
router.post("/", async(req,res) => {
    try {
        const data_rozpoczecia = req.body.data_rozpoczecia;
        const data_zakonczenia = req.body.data_zakonczenia;
        var uzytkownik_id;
        const dane=jwt.decode(req.headers['authorization'])
        if(dane.role!='Manager')//do modyfikacji
        {
            uzytkownik_id=dane.id;
        }
        else
        {

            uzytkownik_id = req.body.uzytkownik_id;
        }
        console.log(uzytkownik_id)
        const zadania_id = req.body.zadania_id
        const status_id = req.body.status_id
        sequelize.models.Zadania_uzytkownika.create({
            data_rozpoczecia:data_rozpoczecia,
            data_zakonczenia:data_zakonczenia,
            uzytkownik_id:uzytkownik_id,
            status_id:status_id,
            zadania_id:zadania_id
        })
        res.status(200).send({message:"Poprawnie dodano zadania uzytkownika"})
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})

    }

})
router.put("/:id",async(req,res) => {
    
    const id = req.params.id
    try
    {
        const id = req.body.id
        const data_rozpoczecia = req.body.data_rozpoczecia
        const data_zakonczenia = req.body.data_zakonczenia
        const uzytkownik_id = req.body.uzytkownik_id
        const zadania_id = req.body.zadania_id
        const status_id = req.body.status_id
        sequelize.models.Zadania_uzytkownika.upsert({
            id:id,
            data_rozpoczecia:data_rozpoczecia,
            data_zakonczenia:data_zakonczenia,
            uzytkownik_id:uzytkownik_id,
            zadania_id:zadania_id,
            status_id:status_id
            
        })
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})
router.delete("/:id",async(req,res) => {
    const id =  req.params.id
    try
    {
        const userTasks = await sequelize.models.Zadania_uzytkownika.findByPk(id)
        userTasks.destroy()
        res.status(200).send({message:"Pomyslnie usunieto zadanie uzytkownika"})
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})
router.get("/",async(req,res) => {
    try 
    {
        userTasks = await sequelize.models.Zadania_uzytkownika.findAll()
        res.status(200). send({data:users,message:"Pomyslnie pobrano liste zadan uzytkownikow"})
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})
router.get("/:id",async(req,res) => {
    id = req.params.id
    try
    {   
        user = await sequelize.models.Zadania_uzytkownika.findByPk(id)
        res.status(200).send({data:user,message:"Pomyslnie pobrano zadanie uzytkownika"})
    }
    catch(error)
    {
         res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})


module.exports = router 