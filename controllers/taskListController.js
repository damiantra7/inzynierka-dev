const router = require('express').Router()
const sequelize = require('../db')
const { route } = require('./vehiclesController')
const jwt =require('jsonwebtoken')
const tasks = require('../model/tasks')

router.get("/",async(req,res) => {
    const dane=jwt.decode(req.headers['authorization'])
    try 
    {
        if(dane.role == "Manager")
        {
            userTasks = await sequelize.models.Zadania_uzytkownika.findAll()
        }
        else
        {
            userTasks = await sequelize.models.Zadania_uzytkownika.findAll({where:{uzytkownik_id:dane.id}})
        }
        
        var tasks = []
        for(var i =0;i<userTasks.length;i++)
        {
            task = await sequelize.models.Zadania.findByPk(userTasks[i].zadania_id)
            statuss = await sequelize.models.Status.findByPk(userTasks[i].status_id)
            pracownik = await sequelize.models.Uzytkownik.findByPk(userTasks[i].uzytkownik_id)
            tasks.push({
                id:userTasks[i].id,
                zadania_id:task.id,
                nazwa:task.nazwa,
                datar:userTasks[i].data_rozpoczecia,
                dataz:userTasks[i].data_zakonczenia,
                priorytet:task.priorytet,
                status_id:statuss.id,
                status:statuss.nazwa,
                opis:task.opis,
                pracownik_id:pracownik.id,
                pracownik:pracownik.imie + " " + pracownik.nazwisko,
                usertask:userTasks[i].id
            })
        }
        console.log(tasks)
        res.status(200). send({data:tasks,message:"Pomyslnie pobrano liste zadan"})
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }   
})
router.post("/",async(req,res)=>{
    const dane=jwt.decode(req.headers['authorization'])
    const id=req.body.id;
    var pracownik = 0; 
    if(dane.role == "Manager")
    {
        pracownik=req.pracownik
    }
    else
    {
        pracownik=dane.id
    }
    req.body.pracownik;
    const nazwa = req.body.nazwa
    const opis = req.body.opis
    const priorytet = req.body.priorytet
    const powiadomienia = req.body.powiadomienia
    const category = req.body.category
    const status = req.body.status
    const datar = req.body.datar
    const dataz =req.body.dataz
    try
    {
        task = await sequelize.models.Zadania.create(
        {
            nazwa:nazwa,
            opis:opis,
            priorytet:priorytet,
            powiadomienia:powiadomienia,
            category_id:category,
            powiadomienia:0
        }
        )
        user_task = await sequelize.models.Zadania_uzytkownika.create({
            data_rozpoczecia:datar,
            data_zakonczenia:dataz,
            uzytkownik_id:2,
            zadania_id:task.id,
            status_id:status
        })
    
        res.status(200).send({message:"Pomyslnie dodano zadaine do bazy danych",id:task.id})
        console.log("Dziala")
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
    
})
router.put("/",async(req,res)=>{
    const id=req.body.id;    
    const usertask_id=req.body.usertask
    try{
        task = await sequelize.models.Zadania.findByPk(id)
        task.nazwa=req.body.nazwa
        task.opis=req.body.opis
        task.priorytet=req.body.priorytet
        task.powiadomienia=0;
        task.category=1;
        task.save()
        user_task = await sequelize.models.Zadania_uzytkownika.findByPk(usertask_id)
        user_task.data_rozpoczecia=req.body.datar
        user_task.data_zakonczenia=req.body.dataz
        user_task.uzytkownik_id=parseInt(req.body.pracownik)
        user_task.zadania_id=id
        user_task.status_id=req.body.status
        user_task.save()
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
   
})
router.delete("/",async(req,res) => {
    id=req.body.id
    zadania_id=req.body.zadania_id
    try
    {
        const userTasks = await sequelize.models.Zadania_uzytkownika.findByPk(id)
        userTasks.destroy();
        const task =await sequelize.models.Zadania.findByPk(zadania_id)
        task.destroy
        res.status(200).send({message:"Pomyslnie usunieto zadanie"})
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    } 
})
module.exports = router