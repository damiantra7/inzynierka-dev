const router = require('express').Router()
const sequelize = require('../db')
const bcrypt = require('bcrypt')
const user = require('../model/user')
const role = require('../model/role')
const validator = require('validator')
router.post("/", async(req,res) => {
    try {
        const u_haslo = req.body.haslo
        correct=true
        err_message=''
        if(!validator.isEmail(req.body.biznes_mail))
        {
            correct=false
            err_message+=' Podano niepoprawny email \n'
        }
        if(!validator.isEmail(req.body.prywatny_email))
        {
            correct=false
            err_message+=" Podano niepoprany email \n"
        }
        if(validator.isEmpty(req.body.imie))
        {
            correct=false
            err_message+=" Podano puste imie \n"
        }
        if(validator.isEmpty(req.body.nazwisko))
        {
            correct=false
            err_message+=" Podano puste nazwisko \n"
        }
        if(!validator.isMobilePhone(req.body.numer_tel))
        {
            correct=false
            err_message+="Podano nie poprawny numer \n"
        }
        if(validator.isEmpty(u_haslo))
        {
            correct=false
            err_message+="Nie podano hasła \n"
        }
        if(correct==false)
        {
            res.status(500).send({message:err_message})
        }
        else
        {
        const salt = await bcrypt.genSalt(10)
        const haslo = await bcrypt.hash(u_haslo, salt)
        sequelize.models.Uzytkownik.create({
            imie:req.body.imie,
            nazwisko:req.body.nazwisko, 
            prywatny_email:req.body.prywatny_email,
            biznes_mail:req.body.biznes_mail,
            numer_tel:req.body.numer_tel,
            haslo:haslo,
            salt:salt,
            role_id: parseInt(req.body.role_id),
            address_id:req.body.address_id
        })
        res.status(200).send({message:"Poprawnie dodano uzytkownika"})
        }
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})

    }
})
router.put("/:id",async(req,res) => {
    
    const id = req.params.id
    try
    {
        const imie = req.body.imie
        const nazwisko = req.body.nazwisko
        const prywatny_email = req.body.prywatny_email
        const biznes_mail = req.body.biznes_mail
        const numer_tel = req.body.numer_tel
        const u_haslo = req.body.haslo
        const salt = await bcrypt.genSalt(10)
        const haslo = await bcrypt.hash(u_haslo, salt)
        const role_id = await sequelize.models.Role.findOne({where:{name:id.body.role}})
        const address_id = req.body.address_id
        sequelize.models.Uzytkownik.upsert({
            id:id,
            imie:imie,
            nazwisko:nazwisko,
            prywatny_email:prywatny_email,
            biznes_mail:biznes_mail,
            numer_tel:numer_tel,
            haslo:haslo,
            salt:salt,
            role_id:role_id,
            address_id:address_id
            
        })
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})
router.delete("/:id",async(req,res) => {
    const id =  req.params.id
    try
    {
        const user = await sequelize.models.Uzytkownik.findByPk(id)
        const adres = await sequelize.models.Adres.findByPk(user.address_id)
        user.destroy()
        adres.destroy()
        res.status(200).send({message:"Pomyslnie usunieto uzytkownika"})
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})
router.get("/",async(req,res) => {
    try 
    {
        users = await sequelize.models.Uzytkownik.findAll()
        var usersf= []
        for(var i =0;i<users.length;i++)
        {
            var role = await sequelize.models.Role.findByPk(users[i].role_id)
            usersf.push(
            {
                id:users[i].id,
                imie:users[i].imie,
                nazwisko:users[i].nazwisko,
                biznes_mail:users[i].biznes_mail,
                numer_tel:users[i].numer_tel,
                role_id:users[i].role_id,
                role:role.nazwa
                
            })  
            console.log(usersf)
        }
        
        res.status(200).send({data:usersf,message:"Pomyslnie pobrano liste uzytkownikow"})
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})
router.get("/:id",async(req,res) => {
    id = req.params.id
    try
    {   
        user = await sequelize.models.Uzytkownik.findByPk(id)
        res.status(200).send({data:user,message:"Pomyslnie pobrano uzytkoqwnika"})
    }
    catch(error)
    {
         res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})
router.put("/role/:id",async(req,res)=>{
    id = parseInt(req.params.id)
    try
    {   
        var user = await sequelize.models.Uzytkownik.findByPk(id)
        user.role_id=parseInt(req.body.role)
        await user.save()
        res.status(200).send({data:user,message:"Pomyslnie zmienione role uzytkownika z "+req.body.role+" na "+user.role_id})
    }
    catch(error)
    {
         res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})
router.put("/edit/:id",async(req,res)=>{
    id = parseInt(req.params.id)
    try
    {   
        var user = await sequelize.models.Uzytkownik.findByPk(id)
        user.imie=req.body.imie
        user.nazwisko=req.body.nazwisko
        user.numer_tel=req.body.tel
        user.biznes_mail=req.body.email
        await user.save()
        res.status(200).send({message:"Pomyslnie zmienione dane uzytkownika"})
    }
    catch(error)
    {
         res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})
module.exports = router 