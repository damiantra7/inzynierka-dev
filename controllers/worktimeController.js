const router = require('express').Router()
const sequelize = require('../db')
const { route } = require('./vehiclesController')
router.post("/startWork",async(req,res)=>{
    try
    {
        const czas_rozpoczecia = req.body.czas_rozpoczecia
        const uzytkownik_id = req.body.uzytkownik_id
        sequelize.models.Worktime.create({
            czas_rozpoczecia:czas_rozpoczecia,
            uzytkownik_id:uzytkownik_id
        })
        res.status(200).send({message:"Poprawnie rozpoczeto dzien pracy"})
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})

router.post("/endWork",async(req,res)=>{
    try
    {
        res.status(200).send({message:"Poprawnie zakonczono dzien pracy"})
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})
module.exports = router