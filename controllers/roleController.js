const router = require('express').Router()
const { response } = require('express')
const sequelize = require('../db')



router.post('/',async(req,res)=>{
    try
    {
    const nazwa = req.body.nazwa
    sequelize.models.Role.create({
        nazwa:nazwa    
    })
    res.status(200).send({message:'Dodano nowa role'})
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})
router.put('/:id',async(req,res)=>{
    id=req.params.id
    try
    {
        const nazwa = req.body.nazwa
        sequelize.models.Role.upsert({
            id:id,
            nazwa:nazwa
        })
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})
router.get("/",async(req,res) => {
   try
   {
        roles=await sequelize.models.Role.findAll()
        res.status(200).send({data:roles,message:"Pobrano liste rol"})
   }
   catch(error)
   {
    res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
   }
})
router.get("/:id",async(req,res) => {
    id=req.params.id
    try
    {   
        role=await sequelize.models.Role.findByPK(id)
        req.status(200).send({data:role,message:"Pobrano role"})

    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})
router.delete("/:id",async(req,res)=>{
    id=req.params.id
    try
    {
        const role = await sequelize.models.Role.findByPk(id)
        role.destroy()
        res.status(200).send({message:"Pomyślnie usunieto role"})
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})

module.exports = router