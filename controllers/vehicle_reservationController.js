const router = require('express').Router()
const sequelize = require('../db')
const { route } = require('./vehiclesController')

router.post("/",async(req,res)=>{
    const dzien_rezerwacji = req.body.dzien_rezerwacji
    const pojazdy_id = req.body.pojazdy_id
    const uzytkownik_id =req.body.uzytkownik_id
    try
    {
        sequelize.models.Rezerwacja_pojazdów.create({dzien_rezerwacji:dzien_rezerwacji,pojazdy_id:pojazdy_id,uzytkownik_id:uzytkownik_id})
        //await Pojazdy.create({marka:mar 

        res.status(200).send({message:"Pomyslnie dodano rezerwacje pojazdow do bazy danych"})
        console.log("Dziala")
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
    
})
router.delete("/:id",async(req,res)=>{
    const id = req.params.id
    try
    {
       const rezerwacja = await sequelize.models.Rezerwacja_pojazdów.findByPk(id)
       rezerwacja.destroy()
        res.status(200).send({message:"Pomyslnie usunieto rezerwacje z bazy danych"})
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})
router.get("/",async(req,res) =>
{
    try
    {
        const rezerwacje = await sequelize.models.Rezerwacja_pojazdów.findAll();
        res.status(200).send({data:rezerwacje,message:"Pobrano liste rezerwacji"});
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})

router.put("/:id",async(req,res)=>{
    id=req.params.id
    try
    {
        const dzien_rezerwacji = req.body.dzien_rezerwacji
        const pojazdy_id = req.body.pojazdy_id
        const user_id = req.body.user_id
        sequelize.models.Rezerwacja_pojazdów.upsert({
           dzien_rezerwacji:dzien_rezerwacji,pojazdy_id:pojazdy_id,user_id:user_id
        })
        res.status(200).send({message:"Pomyslnie zaktualizowano pojazd"})
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})
module.exports = router