const router = require('express').Router()
const sequelize = require('../db')
const user = require('../model/user')

router.get("/",async(req,res) => {
    try 
    {
        users = await sequelize.models.Uzytkownik.findAll()
        res.status(200). send({data:users,message:"Pomyslnie pobrano liste uzytkownikow"})
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})

module.exports = router 