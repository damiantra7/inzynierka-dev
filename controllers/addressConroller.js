const router = require('express').Router()
const { response } = require('express')
const sequelize = require('../db')

router.post("/",async(req,res) => {
    try{
        const nr_domu = req.body.nr_domu
        const nr_mieszkania = req.body.nr_mieszkania
        const ulica = req.body.ulica
        const kod_pocztowy = req.body.kod_pocztowy
        const miasto = req.body.miasto
        const wojewodztwo=req.body.wojewodztwo
        const address = await sequelize.models.Adres.create({
            nr_domu:nr_domu,
            nr_mieszkania:nr_mieszkania,
            ulica:ulica,
            kod_pocztowy:kod_pocztowy,
            miasto:miasto,
            wojewodztwo:wojewodztwo
        })
        res.status(200).send({id:address.id,message:"Poprawnie dodano adres"})
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})

router.put("/:id",async(req,res) => {
    id=req.params.id
    try{
        const nr_domu = req.body.nr_domu
        const nr_mieszkania = req.body.nr_mieszkania
        const ulica = req.body.ulica
        const kod_pocztowy = req.body.kod_pocztowy
        const miasto = req.body.miasto
        const wojewodztwo=req.body.wojewodztwo
        sequelize.models.Adres.upsert({
            id:id,
            nr_domu:nr_domu,
            nr_mieszkania:nr_mieszkania,
            ulica:ulica,
            kod_pocztowy:kod_pocztowy,
            miasto:miasto,
            wojewodztwo:wojewodztwo
        })
        res.status(200).send({message:"Poprawnie dodano adres"})
    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})
router.get("/:id",async(req,res)=>{
    id=req.params.id
    try
    {
        adres=await sequelize.models.Adres.findByPk(id)
        res.status(200).send({data:adres,message:"Pobrano adres"})

    }
    catch(error)
    {
        res.status(500).send({message:"Wewnetrzny blad serwera! "+error})
    }
})
module.exports = router